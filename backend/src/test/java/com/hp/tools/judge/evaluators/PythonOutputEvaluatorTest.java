package com.hp.tools.judge.evaluators;

import com.hp.tools.judge.TestUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PythonOutputEvaluatorTest {
    private OutputEvaluator evaluator;

    @Before
    public void setUp() throws Exception {
        evaluator = OutputEvaluatorFactory.buildEvaluator("python");
    }

    @After
    public void tearDown() throws Exception {
        evaluator = null;
    }

    @Test
    public void testEvaluate() throws Exception {
        String contents = TestUtils.getFileContents("python-output.txt");
        float score = evaluator.evaluate(contents);

        Assert.assertEquals(score, 40f, 0.1);
    }
}