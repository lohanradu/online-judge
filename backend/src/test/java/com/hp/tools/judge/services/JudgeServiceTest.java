package com.hp.tools.judge.services;

import com.hp.tools.judge.entities.ProgrammingProblemDetails;
import com.hp.tools.judge.entities.Score;
import com.hp.tools.judge.entities.Solution;
import com.hp.tools.judge.entities.User;
import com.hp.tools.judge.exceptions.OnlineJudgeException;
import com.hp.tools.judge.utils.ConnectionDetails;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JudgeServiceTest {
    private static final String TEST_LANGUAGE = "java";
    private static final String TEST_CODE = "//test code";
    private static final String TEST_SCORE_DESCRIPTION = "Success";
    private static final String TEST_LAST_RESULT = "N/A";
    private static final String TEST_EMAIL = "a.b@gmail.com";

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private JudgeService service;

    @Before
    public void setUp() throws Exception {
        service = new JudgeService();
    }

    @After
    public void tearDown() throws Exception {
        service = null;
    }

    @Test
    public void testGetProblemsDetailsNoSolutions() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        List<ProgrammingProblemDetails> problems = service.getAllProgrammingProblemDetails(0);

        ProgrammingProblemDetails firstProblem = problems.get(0);
        Assert.assertEquals(firstProblem.getId(), 1);
        Assert.assertEquals(firstProblem.getScore(), 0f, 0.1);
        Assert.assertEquals(firstProblem.getLastResult(), TEST_LAST_RESULT);
        Assert.assertEquals(problems.size(), 3);
    }

    @Test
    public void testGetProblemsDetailsWithSolutions() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        Solution solution = new Solution();
        solution.setUserId(user.getId());
        solution.setProblemId(1);
        solution.setCode(TEST_CODE);
        solution.setLanguage(TEST_LANGUAGE);
        solution.setScore(new Score(100f, TEST_SCORE_DESCRIPTION));
        service.saveSolution(solution);

        List<ProgrammingProblemDetails> problems = service.getAllProgrammingProblemDetails(0);

        ProgrammingProblemDetails firstProblem = problems.get(0);
        Assert.assertEquals(firstProblem.getId(), 1);
        Assert.assertEquals(firstProblem.getScore(), 0f, 0.1);
        Assert.assertEquals(firstProblem.getLastResult(), "0.0 - Not evaluated");
        Assert.assertEquals(problems.size(), 3);
    }

    @Test
    public void testSaveSolution() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        Solution solution = new Solution();
        solution.setUserId(user.getId());
        solution.setProblemId(1);
        solution.setCode(TEST_CODE);
        solution.setLanguage(TEST_LANGUAGE);
        solution.setScore(new Score(100f, TEST_SCORE_DESCRIPTION));
        service.saveSolution(solution);

        List<Solution> solutions = service.getSolutions(0, 1);

        Assert.assertEquals(solutions.size(), 1);
        Solution firstSolution = solutions.get(0);
        Assert.assertEquals(firstSolution.getScore().getValue(), 0f, 0.1);
        Assert.assertEquals(firstSolution.getScore().getDescription(), "Not evaluated");
    }

    @Test
    public void testGetSolutionsInvalidUserId() throws Exception {
        exception.expect(OnlineJudgeException.class);
        exception.expectMessage("Invalid user ID: 0");

        service.getSolutions(0, 1);
    }

    @Test
    public void testGetSolutionsInvalidProblemId() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        exception.expect(OnlineJudgeException.class);
        exception.expectMessage("Invalid problem ID: -1");

        service.getSolutions(0, -1);
    }

    @Test
    public void testCheckSolutionInvalidSolutionId() throws Exception {
        exception.expect(OnlineJudgeException.class);
        exception.expectMessage("Invalid solution ID: -1");

        service.scoreSolution(-1, null);
    }

    @Test
    public void testCheckSolution() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        Solution solution = new Solution();
        solution.setUserId(user.getId());
        solution.setProblemId(1);
        solution.setCode(TEST_CODE);
        solution.setLanguage(TEST_LANGUAGE);
        service.saveSolution(solution);

        ConnectionDetails connectionDetails = new ConnectionDetails("http", "127.0.0.1", 8080, "agent/test", 3000, 10000);
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        service.scoreSolution(0, new AgentService(connectionDetails, threadPool));

        Thread.sleep(5000);

        List<ProgrammingProblemDetails> problems = service.getAllProgrammingProblemDetails(0);
        ProgrammingProblemDetails firstProblem = problems.get(0);
        Assert.assertEquals(firstProblem.getId(), 1);
        Assert.assertEquals(firstProblem.getScore(), 0f, 0.1);
        Assert.assertEquals(firstProblem.getLastResult(), "0.0 - Agent Generic Error");

        threadPool.shutdown();
    }

    @Test
    public void testFindUserByEmail() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        User expectedUser = service.findUserByEmail(TEST_EMAIL);

        Assert.assertEquals(user, expectedUser);
    }

    @Test
    public void testSaveUserWithTheSameEmail() throws Exception {
        User user = getTestUser();
        service.saveUser(user);

        exception.expect(OnlineJudgeException.class);
        exception.expectMessage("This email " + TEST_EMAIL + " is already used");

        user = getTestUser();
        service.saveUser(user);
    }

    private User getTestUser() {
        User user = new User();
        user.setFirstName("A");
        user.setLastName("B");
        user.setEmail(TEST_EMAIL);
        return user;
    }
}