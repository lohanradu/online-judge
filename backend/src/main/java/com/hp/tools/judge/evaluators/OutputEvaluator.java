package com.hp.tools.judge.evaluators;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public interface OutputEvaluator {
    float evaluate(String output);
}
