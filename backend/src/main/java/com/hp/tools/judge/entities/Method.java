package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 06.03.2015
 */
public class Method {
    private String language;
    private String signature;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
