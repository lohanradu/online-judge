package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 06.03.2015
 */
public class ProgrammingProblemDetails extends ProgrammingProblem {
    private float score;
    private String lastResult;

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getLastResult() {
        return lastResult;
    }

    public void setLastResult(String lastResult) {
        this.lastResult = lastResult;
    }
}
