package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 20.02.2015
 */
public class AgentResult {
    private int returnCode;
    private String stdout;
    private String stderr;

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getStderr() {
        return stderr;
    }

    public void setStderr(String stderr) {
        this.stderr = stderr;
    }
}
