from UUTModule import *
import unittest

SYMMETRIC = "symmetric"
NOT_SYMMETRIC = "not symmetric"
NOT_POSSIBLE = "not possible"
class MatrixSymmetryTests(unittest.TestCase):
    def testSymmetry1(self):
        self.assertEquals(NOT_POSSIBLE, isSymmetric(["1","1","1","1","<>","0","1","0","1","<>","1","1","0","1"]))


    def testSymmetry2(self):

        self.assertEquals(SYMMETRIC, isSymmetric(["1","0","1","<>","0","1","0","<>","1","0","1"]))

    def testSymmetry3(self):

        self.assertEquals(NOT_SYMMETRIC, isSymmetric(["1","0","1","<>","0","1","0","<>","1","14","1"]))

    def testSymmetry4(self):

        self.assertEquals(NOT_POSSIBLE, isSymmetric(["1","0","1","0","1","0","<>","1","14","1"]))


    def testSymmetry5(self):

        self.assertEquals(NOT_SYMMETRIC, isSymmetric(["1","141","1","<>","0","1","0","<>","1","14","1"]));



def main():
    unittest.main()

if __name__ == '__main__':
    main()