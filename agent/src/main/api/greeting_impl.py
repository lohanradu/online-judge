import json

HELLO_MSG = "Hello I am a cool code testing agent."


class Greeting:
    def GET(self):
        return json.dumps({"Status": 0, "Message": HELLO_MSG})
