'use strict'

app.service('problemsService', function() {
                                    
    var problemIndex = "";
    var problems = "";
    
    this.setProblemIndex = function(index) {
        this.problemIndex = index;   
    }
    
    this.getProblemIndex = function() {
        return this.problemIndex;
    }                                    
                      
    this.setProblems = function(data) {
        this.problems = data;
    }
    
    this.getProblems = function() {
        return this.problems;
    }
    
});